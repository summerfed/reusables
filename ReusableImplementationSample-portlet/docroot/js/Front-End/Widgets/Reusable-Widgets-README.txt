This README file explains what are the contents of this reusable front-end codes.
Author: Fed Manangan
Co-Author/s:
Date Created: 2016-12-21 01:58PM
Last Modified: 2016-12-21 01:58PM

How to use in Liferay Portlet:
Make sure you include jquery javascript file together with chosen module javascript file in liferay-portlet.xml. Example shown below:

<portlet>
	<portlet-name>reusable-implementation-sample</portlet-name>
	<icon>/icon.png</icon>
	<header-portlet-css>/css/main.css</header-portlet-css>
	
	/** THIS IS THE JQUERY IMPORT
	<footer-portlet-javascript>
		/js/Front-End/jquery-2.2.3.min.js
	</footer-portlet-javascript>
	
	/** THIS MODULE JAVASCRIPT IMPORT
	<footer-portlet-javascript>
		/js/Front-End/inputbox-formatter.js
	</footer-portlet-javascript>
	
	/** THIS MODULE JAVASCRIPT DEPENDENCY IMPORT
	<footer-portlet-javascript>
		/js/Front-End/Javascript/data-utils.js
	</footer-portlet-javascript>
	
	<css-class-wrapper>
		reusable-implementation-sample-portlet
	</css-class-wrapper>
</portlet>
 
LEGEND:
values enclosed with {} will serve as parameters and will be replaced with specific values.

Start of Module: =={module title}==
* : function

==MODALS==
description: Contains reusable code for Web Browser Modals 
file location: /Front-End/Widgets/modals.js 
dependencies: 
--jquery-2.2.3.min.js or Higher
--data-utils.js

*LoadingModal
