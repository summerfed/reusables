/**
 * Cross-browser and mobile friendly function to get keyboard event keyCode
 * @param event = event parameter example shown below:
 * element.keypress(function(event){
		var key = getKeyCode(event);
	 });
 * @returns key = keycode equivalent of pressed key
 */
function getKeyCode(event) {
	var key = (typeof event.which == "number") ? event.which : event.keyCode;
	if (key == 0 || key == 229) { //for android chrome keycode fix
		key = getCorrectKeyCode($(event.target).val());
	}
	return key;
}

