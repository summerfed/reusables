/**
 * Limit number of accepted characters of HTML input boxes.
 * @param element = jquery selected element for example $('#elementId'), element can be any type of input box.
 * @param limit = integer value of limit.
 */
function setInputBoxCharLimit(element, limit) {
	element.attr('maxlength', limit);
	element.oninput = function() {
		if (this.value.length > this.maxLength) {
			this.value = this.value.slice(0, this.maxLength);
		}
	};
	oninput = "javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
}

/**
 * Limit what characters the box will accept based on provided parameters.
 * @param element = jquery selected element for example $('#elementId'), element can be any type of input box.
 * @param characters {String} = if you want to allow numbers only pass '1234567890'
 */
function allowCharacters(element, characters) {
	 element.unbind('keypress');
	 element.keypress(function(event){
		 	var key = getKeyCode(event);
			var char = String.fromCharCode(key);
			var isAcceptedCharacter = characters.indexOf(char) !== -1;
			var isSpace = key == 32;
			if(isAcceptedCharacter) {
				return key;
			} else {
				return false;
			}
	 });
}

/**
 * Make input box mobile number formatted, ensure country code is present on blur if enabled.
 * @param element = jquery selected element for example $('#elementId'), element can be any type of input box 
 * but recommended is tel for mobile friendly implementation.
 * @param enableCountryCode = true or false, if set to true then country code will be added on blur.
 * @param countryCode = integer number of country code for ex. Philippines = 63.
 */
function setAsMobileNumberTextBox(element, enableCountryCode, countryCode) {
	 var numbers = "0123456789";
	 element.unbind('keypress');
	 element.keypress(function(event){
	 		/*
	 		 * 48 to 57 = 0 to 9
	 		 * 43 = +
	 		 * 8 = backspace
	 		 */
			var key = getKeyCode(event);
			if((key >= 48 && key <= 57)) {
				return key;
			} else if(key == 43) {
				return key
			} else if(key == 8) {
				return key;
			} else {
				return false;
			}
	 });
	 
	 if(enableCountryCode) {
		 element.unbind('blur');
		 element.blur(function(event){
				var value = $(this).val();
				var firstChar = value[0];
				var firstTwoChar = value[0]+value[1];
				if(firstChar == '0') {
					value = '+'+countryCode + value.substring(1, value.length);
				} else if(firstTwoChar == countryCode){
					value = '+' + value;
				} else if(numbers.indexOf(firstChar) > -1) {
					value = '+'+countryCode + value;
				}
				element.val(value);
		 });
	 }
}

/**
 * Set input box to accept letter input and symbols: -#.ñÑ only, these are the values set as standard for names.
 * @param element = jquery selected element for example $('#elementId'), element should be input box type text.
 */
function setAsNameTextBox(element) {
	 var symbols = "`-#.ñÑ";
	 element.unbind('keypress');
	 element.keypress(function(event){
	 		/*
	 		 * 65 to 90 = A to Z
	 		 * 8 = backspace
	 		 */
			var key = getKeyCode(event);
			var char = String.fromCharCode(key);
			var isLetter = (key >= 65 && key <= 90) || (key >= 97 && key <= 122);
			var isAcceptedSymbol = symbols.indexOf(char) !== -1;
			var isSpace = key == 32;
			if(isLetter) {
				return key;
			} else if(isAcceptedSymbol) {
				return key;
			} else if(key == 8) {
				return key;
			} else if(key == 46) {
				return key;
			} else if(isSpace){
				return key;
			} else {
				return false;
			}
	 });
}

/**
 * Set input box to accept numbers and decimal point input only, add comma separator as you type values.
 * @param element = jquery selected element for example $('#elementId'), element should be type tel for mobile friendly implementation.
 */
function setAsCurrencyTextBox(element) {
	var beforeEdit = element.val();
	element.unbind('keypress');
 	element.keypress(function(event){
 		/*
 		 * 48 to 57 = 0 to 9
 		 * 46 = .
 		 * 8 = backspace
 		 */
		var key = getKeyCode(event);
		if((key >= 48 && key <= 57)) {
			limitDecimalPlaceInput(key);
			return key;
		} else if(key == 46) {
			addPeriod();
		} else if(key == 8) {
			return key;
		} else {
			return false;
		}
		
		function addPeriod() {
			if(hasDecimal()) {
				event.preventDefault();
				return false;
			} else {
				return key;
			}
		}
		
		function limitDecimalPlaceInput(key) {
			if(inDecimalLocation()) {
				var value = element.val();
				var indexOfDecimalPoint = value.indexOf('.');
				var indexOfDecimalValue = indexOfDecimalPoint+1;
				var decimalValue = value.substring(indexOfDecimalValue, value.length);
				var currentCursor = element.getCursorPosition();
				var length = value.length;
				
				if((length - currentCursor) == 1) {
					var char = String.fromCharCode(key);
					var first = value.substr(0, value.length-1);
					element.val(first + char);
				}
				
				if(decimalValue.length<=1) {
					return key;
				} else {
					event.preventDefault();
					return false;
				}
			}
		}
	}).keyup(function(event){
		var key = getKeyCode(event);
		var formattedAmount = '';
		var isDecimalKey = (key == 110);
		var isNumberKey = (key >= 48 && key <= 57) || (key >= 96 && key <= 105);
		var isDelete = (key==8 || key == 46);
		var curCursorLocation = element.getCursorPosition();
		var value = element.val();
		var valLength = value.length;
		
		if(isDelete) {
			curCursorLocation -= 1;
		}
		
		if(!inDecimalLocation()) {
			if(!isDecimalKey) {
				formattedAmount = formatAmount();
				if(!hasDecimal()) {
					formattedAmount = formattedAmount.substring(0,formattedAmount.length-3);
				}
			}
		} 
		
		if(formattedAmount.length>0) {
			element.val(formattedAmount);
			var newValLength = formattedAmount.length;
			var lengthDifference = newValLength - valLength;
			if(lengthDifference == 0) {
				if(isDelete) {
					curCursorLocation += 1;
				}
			} else if(lengthDifference == 1) {
				if(isNumberKey) {
					curCursorLocation += 1;
				}
			}
			element.setCursorPosition(curCursorLocation);
		}
		
	}).blur(function(event){
		var formattedAmount = formatAmount();
		if(formattedAmount.length>0) {
			element.val(formattedAmount);
		} 
	});
 	
 	function formatAmount() {
 		var value = element.val();
		var inputAmountValue = value.replace(/,/g, '');	
		var formattedAmount = Number(inputAmountValue).formatMoney(2);
		if(formattedAmount == '0.00') {
			formattedAmount = '';
		}
		return formattedAmount;
 	}
 	
 	function hasDecimal() {
 		var value = element.val();
		var indexOfDecimalPoint = value.indexOf('.');
		var hasDecimal = indexOfDecimalPoint>-1;
		return hasDecimal;
 	}
 	
 	function inDecimalLocation() {
 		var value = element.val();
		var indexOfDecimalPoint = value.indexOf('.');
		var isInDecimal = (value.length - element.getCursorPosition())<=1 && hasDecimal();
		return isInDecimal;
 	}
}

//SET CURSOR POSITION
//SAMPLE USAGE $('#elementId').setCursorPosition(1);
$.fn.setCursorPosition = function(pos) {
	  this.each(function(index, elem) {
	    if (elem.setSelectionRange) {
	      elem.setSelectionRange(pos, pos);
	    } else if (elem.createTextRange) {
	      var range = elem.createTextRange();
	      range.collapse(true);
	      range.moveEnd('character', pos);
  range.moveStart('character', pos);
  range.select();
}
});
return this;
};	

//GET CURSOR POSITION
//SAMPLE USAGE $('#elementId').getCursorPosition();
(function($) {
$.fn.getCursorPosition = function() {
    var input = this.get(0);
    if (!input) return; // No (input) element found
    if ('selectionStart' in input) {
        // Standard-compliant browsers
        return input.selectionStart;
    } else if (document.selection) {
        // IE
        input.focus();
        var sel = document.selection.createRange();
        var selLen = document.selection.createRange().text.length;
        sel.moveStart('character', -input.value.length);
        return sel.text.length - selLen;
    }
}
})(jQuery);