<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>

<portlet:defineObjects />

<h1>This Contains Demo of Reusable Front End Codes</h1>

<h2>INPUT FORMATTER</h2>
<h3>setInputBoxCharLimit</h3>
<label for="setInputBoxCharLimit">This input box will accept 5 characters only:<br /> <input type="text" id="setInputBoxCharLimit" /></label>
<aui:script>
	/* LIMIT character input in  #setInputBoxCharLimit to 5 characters only*/
	setInputBoxCharLimit($('#setInputBoxCharLimit'), 5);
</aui:script>

<h3>allowCharacters</h3>
<div>Limit what characters the box will accept based on provided parameters.</div>
<input type="text" id="allowedCharacters" placeholder="Enter Allowed Characters"/>
<input type="text" id="allowedCharactersTester" placeholder="This box will accept only the allowed characters." />
<aui:script>
	$('#allowedCharacters').change(function(){
		var element = $('#allowedCharactersTester');
		var symbols = $(this).val();
		allowCharacters(element, symbols);
	});
</aui:script>

<h3>setAsMobileNumberTextBox</h3>
<label for="setAsMobileNumberTextBox">This input box will ensure that country code 63 is present on leave:<br /> <input type="tel" id="setAsMobileNumberTextBox" /></label>
<aui:script>
	/* This input box will ensure that country code 63 is present on leave */
	setAsMobileNumberTextBox($('#setAsMobileNumberTextBox'), true, 63);
</aui:script>

<h3>setAsNameTextBox</h3>
<label for="setAsNameTextBox">Set input box to accept letter input and symbols: -#.�� only, these are the values set as standard for names:<br /> <input type="text" id="setAsNameTextBox" /></label>
<aui:script>
	/* This input box will ensure that country code 63 is present on leave */
	setAsNameTextBox($('#setAsNameTextBox'));
</aui:script>

<h3>setAsCurrencyTextBox</h3>
<label for="setAsCurrencyTextBox">Set input box to accept numbers and decimal point input only, add comma separator as you type values:<br /> <input type="tel" id="setAsCurrencyTextBox" /></label>
<aui:script>
	/* This input box will ensure that country code 63 is present on leave */
	setAsCurrencyTextBox($('#setAsCurrencyTextBox'));
</aui:script>

<h2>DATA UTILITIES</h2>

<h3>Narrower</h3>
<label for="setAsCurrencyTextBox">
	Narrow an array based on set filter:
	<br /> 
	<input type="search" id="narrowerFilter" placeholder="Enter values to filter result"/>
	<br/>
	<div><span>Result: </span><span id="narrowerResultCount"></span></div>
	<select multiple id="narrowerResult">
	  <option value="Volvo">Volvo</option>
	  <option value="Saab">Saab</option>
	  <option value="Opel">Opel</option>
	  <option value="Audi">Audi</option>
	  <option value="Toyota">Toyota</option>
	  <option value="Honda">Honda</option>
	  <option value="Hyundai">Hyundai</option>
	  <option value="Suzuki">Suzuki</option>
	  <option value="Chevrolet">Chevrolet</option>
	  <option value="Mitsubishi">Mitsubishi</option>
	  <option value="Ford">Ford</option>
	  <option value="Ferrari">Ferrari</option>
	</select>
</label>


<aui:script>
	var optionVal = new Array();
    $('#narrowerResult option').each(function() {
        optionVal.push($(this).val());
    });
    
    $('#narrowerResultCount').text(optionVal.length);
    
    var narrower = new Narrower(optionVal);
    
    $('#narrowerFilter').keyup(function(){
    	var filter = $(this).val();
    	
    	// get narrowed array based on filter
    	var newOptionVal = narrower.update(filter);
    	var narrowerResultCount = newOptionVal.length;
    	var narrowerResult = $('#narrowerResult');
    	var optionValues = '';
    	$('#narrowerResultCount').text(narrowerResultCount);
    	newOptionVal.forEach(function(value) {
    		optionValues+= '<option value="'+value+'">'+value+'</option>';
    	});
    	
    	narrowerResult.html(optionValues);
    });
</aui:script>

<h3>getUrlParams</h3>
Get Query Parameters from Given Url
<input type="text" id="urlData" />
<br />
<button type="button" id="getQueryParams">Click Me to Read Url Parameters</button>
<br />
<br />
<p>Url Query Parameters</p>
<div id="queryParamValues"></div>
<aui:script>
	var currentUrl = window.location.href;
	currentUrl = currentUrl + '?position=Front End Developer&function=getQueryParameter';
	$('#urlData').val(currentUrl);
	$('#urlData').css('width', '100%');
	
	$('#getQueryParams').click(function(){
		var oldUrl = $('#urlData').val();
		// use getUrlParams() to get parameters from current page url
		var queryParams = getUrlParams(oldUrl);
		var paramHtml = '';
		for (var property in queryParams) {
			var propertyValue = queryParams[property];
			if(propertyValue!== 'undefined') {
				paramHtml = paramHtml +'<span>'+property+' = </span><span>'+propertyValue+'</span><br />';
			}
		}
		
		$('#queryParamValues').html(paramHtml);
	});
</aui:script>

<h3>generateRandomData</h3>
Generate random String with values based on given possibleCharacters and given length
<input type="text" id="possibleCharacters" value="abcdefghijlmnopqrstuvwxyz1234567890"/>
<input type="number" id="length" value="10"/>
<div><span>Generated Random Values: </span><span id="generatedRandomValue"></span></div>
<button id="btnGenerateRandomValues">Click Me to Generate Random Values</button>
<aui:script>
	$('#btnGenerateRandomValues').click(function(){
		var possibleCharacters = $('#possibleCharacters').val();
		var length = $('#length').val();
		var generatedRandomData = generateRandomData(possibleCharacters, length);
		$('#generatedRandomValue').text(generatedRandomData);
	});
</aui:script>

<h2>Widgets</h2>

<h3>Show Loading Screen</h3>
<input type="number" id="loadingModalTimer" placeholder="Enter Milliseconds for timeout error"/>
<p>NOTE: if no error timer value is entered, modal will disappear after 10 seconds to demonstrate calling of modal hide function</p>
<div><span>Loading Image Location: </span><span id="loadingImageLocation"></span></div>
<br />
<button type="button" id="showLoadingScreen">Show Blocker Modal</button>
<aui:script>
	var loadingModal;
	$('#showLoadingScreen').click(function(){
		// Get Image Location
		var imageLoadingSrc = '<%=renderRequest.getContextPath()%>'+'/images/loader.gif';
		var timerCount = $('#loadingModalTimer').val();
		if(timerCount.length==0) {
			timerCount = undefined;
		}
		
		// Create Loading Modal Object
		loadingModal = new LoadingModal(imageLoadingSrc, timerCount);
		
		// Show Modal
		loadingModal.show();
		
		// if no timer is set, hide modal after 10 seconds
		if(timerCount == undefined) {
			setTimeout(function(){
				// Hide Modal
				loadingModal.hide();
			}, 10000);
		}
		
		$('#loadingImageLocation').text(imageLoadingSrc);
	});
</aui:script>
