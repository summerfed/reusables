Number.prototype.formatMoney = function(c, d, t){
var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };
 
 var loadingTimeout;
 
 function showLoadingScreen(contextPath) {
	 
	 var loadingScreenHtml = '<div id="loading" style=" width: 100%;height: 100%;top: 0;left: 0;position: fixed;opacity: 1;background-color: rgba(0,0,0, .2);z-index: 99;text-align: center;">'+
							 	'<img style=" position: absolute;top: 50%;left: 50%;margin-left: -140px;margin-top: -120px;z-index: 100;" id="loading-image" src="'+contextPath+'/images/animations/loading-bar.gif" alt="Loading..." />'+
							 '</div>';
	 
	 if($( "#loading" ).length==0) {
		 $('body').append(loadingScreenHtml);
	 }
	 $('#loading').css('display','block');
	 
	 loadingTimeout = setTimeout(function() {
	 	var isLoadingExist = $('#loading').css('display');
	 	if(isLoadingExist == 'block'){
	 		hideLoadingScreen();
	 		 if($( "#timeoutError" ).length==0) {
	 			 var timeoutError = '<div id="timeoutError" style="   color: white; width: 100%;height: 100%;top: 0;left: 0;position: fixed;opacity: 1;background-color: rgba(0,0,0, .2);z-index: 99;text-align: center;">'+
					'<div id="timeoutErrorContent" style=" background-color: gray;padding: 20px; position: absolute;top: 50%;left: 50%;margin-left: -135px;margin-top: -100px;z-index: 100;"><p>An error has occured please try again.</p> <button type="button" id="closeLoadingError">OK</button></div>'+
					'</div>';
	 			 
	 			 $('body').append(timeoutError);
	 			 
	 			 $('#closeLoadingError').click(function(){
	 				$('#timeoutError').css('display', 'none'); 
	 			 });
	 		 }
	 		 $('#timeoutError').css('display','block');
	 	}
	 }, 60000);
 }
 
 function hideLoadingScreen() {
	 $('#loading').css('display','none');
	 clearTimeout(loadingTimeout);
 }
 
 function setInputBoxCharLimit(element, limit) {
	 element.attr('maxlength', limit);
	 element.oninput = function(){
		 if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);
	 };
	 oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
 }
 
 function setAsMobileNumberTextBox(element) {
	 var numbers = "123456789";
	 element.keypress(function(event){
	 		/*
	 		 * 48 to 57 = 0 to 9
	 		 * 43 = +
	 		 * 8 = backspace
	 		 */
			var key = getKeyCode(event);
			console.log('key: ' + key);
			if((key >= 48 && key <= 57)) {
				return key;
			} else if(key == 43) {
				return key
			} else if(key == 8) {
				return key;
			} else {
				return false;
			}
	 });
	 
	 element.blur(function(event){
		var value = $(this).val();
		var firstChar = value[0];
		var firstTwoChar = value[0]+value[1];
		if(firstChar == '0') {
			value = '+63' + value.substring(1, value.length);
		} else if(firstTwoChar == '63'){
			value = '+' + value;
		} else if(numbers.indexOf(firstChar) > -1) {
			value = '+63' + value;
		}
		element.val(value);
	 });
 }
 
 function setAsNameTextBox(element) {
	 var symbols = "`-#.ñÑ";
	 element.keypress(function(event){
	 		/*
	 		 * 65 to 90 = A to Z
	 		 * 8 = backspace
	 		 */
			var key = getKeyCode(event);
			var char = String.fromCharCode(key);
			var isLetter = (key >= 65 && key <= 90) || (key >= 97 && key <= 122);
			var isAcceptedSymbol = symbols.indexOf(char) !== -1;
			var isSpace = key == 32;
			if(isLetter) {
				return key;
			} else if(isAcceptedSymbol) {
				return key;
			} else if(key == 8) {
				return key;
			} else if(key == 46) {
				return key;
			} else if(isSpace){
				return key;
			} else {
				return false;
			}
	 });
 }
	 
 function setAsCurrencyTextBox(element) {
	var beforeEdit = element.val();
 	element.keypress(function(event){
 		/*
 		 * 48 to 57 = 0 to 9
 		 * 46 = .
 		 * 8 = backspace
 		 */
		var key = getKeyCode(event);
		if((key >= 48 && key <= 57)) {
			limitDecimalPlaceInput(key);
			return key;
		} else if(key == 46) {
			addPeriod();
		} else if(key == 8) {
			return key;
		} else {
			return false;
		}
		
		function addPeriod() {
			if(hasDecimal()) {
				event.preventDefault();
				return false;
			} else {
				return key;
			}
		}
		
		function limitDecimalPlaceInput(key) {
			if(inDecimalLocation()) {
				var value = element.val();
				var indexOfDecimalPoint = value.indexOf('.');
				var indexOfDecimalValue = indexOfDecimalPoint+1;
				var decimalValue = value.substring(indexOfDecimalValue, value.length);
				var currentCursor = element.getCursorPosition();
				var length = value.length;
				
				if((length - currentCursor) == 1) {
					var char = String.fromCharCode(key);
					var first = value.substr(0, value.length-1);
					element.val(first + char);
				}
				
				if(decimalValue.length<=1) {
					return key;
				} else {
					event.preventDefault();
					return false;
				}
			}
		}
	}).keyup(function(event){
		var key = getKeyCode(event);
		var formattedAmount = '';
		var isDecimalKey = (key == 110);
		var isNumberKey = (key >= 48 && key <= 57) || (key >= 96 && key <= 105);
		var isDelete = (key==8 || key == 46);
		var curCursorLocation = element.getCursorPosition();
		var value = element.val();
		var valLength = value.length;
		
		if(isDelete) {
			curCursorLocation -= 1;
		}
		
		if(!inDecimalLocation()) {
			if(!isDecimalKey) {
				formattedAmount = formatAmount();
				if(!hasDecimal()) {
					formattedAmount = formattedAmount.substring(0,formattedAmount.length-3);
				}
			}
		} 
		
		if(formattedAmount.length>0) {
			element.val(formattedAmount);
			var newValLength = formattedAmount.length;
			var lengthDifference = newValLength - valLength;
			if(lengthDifference == 0) {
				if(isDelete) {
					curCursorLocation += 1;
				}
			} else if(lengthDifference == 1) {
				if(isNumberKey) {
					curCursorLocation += 1;
				}
			}
			element.setCursorPosition(curCursorLocation);
		}
		
	}).blur(function(event){
		var formattedAmount = formatAmount();
		if(formattedAmount.length>0) {
			element.val(formattedAmount);
		} 
	});
 	
 	function formatAmount() {
 		var value = element.val();
		var inputAmountValue = value.replace(/,/g, '');	
		var formattedAmount = Number(inputAmountValue).formatMoney(2);
		if(formattedAmount == '0.00') {
			formattedAmount = '';
		}
		return formattedAmount;
 	}
 	
 	function hasDecimal() {
 		var value = element.val();
		var indexOfDecimalPoint = value.indexOf('.');
		var hasDecimal = indexOfDecimalPoint>-1;
		return hasDecimal;
 	}
 	
 	function inDecimalLocation() {
 		var value = element.val();
		var indexOfDecimalPoint = value.indexOf('.');
		var isInDecimal = (value.length - element.getCursorPosition())<=1 && hasDecimal();
		return isInDecimal;
 	}
}
 
 function disableLetterInput(element) {
	element.keypress(function(e){
		var key = getKeyCode(e);
		if((key >= 48 && key <= 57)) {
			return key;
		} else if(key == 8) {
			return key;
		} else {
			return false;
		}
	});
}
 
 function getKeyCode(event) {
		var key = (typeof event.which == "number") ? event.which : event.keyCode;
		if (key == 0 || key == 229) { //for android chrome keycode fix
			key = getCorrectKeyCode($(event.target).val());
		}
		return key;
	}
 
 var Narrower = (function ( window, document, undefined ) {

	    function Narrower( arr ) {
	        this.arr  = arr || []
	    }

	    Narrower.prototype.update = function ( str ) {
	        var rgxp, results

	        // optimization
	        if ( '' === str ) {
	            return this.arr
	        }
	        else {
	            // create regular expression
	            rgxp = new RegExp( str, 'i' ) // todo: make Unicode-safe once ES6 is widely adopted

	            results = this.arr.filter(function ( val ) {
	                return null !== val.match( rgxp )
	            })

	            return results.sort()
	        }
	    }

	    return Narrower

	}).call( this, this, this.document );
	
//SET CURSOR POSITION
//SAMPLE USAGE $('#username').setCursorPosition(1);
$.fn.setCursorPosition = function(pos) {
	  this.each(function(index, elem) {
	    if (elem.setSelectionRange) {
	      elem.setSelectionRange(pos, pos);
	    } else if (elem.createTextRange) {
	      var range = elem.createTextRange();
	      range.collapse(true);
	      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  });
  return this;
};	

(function($) {
    $.fn.getCursorPosition = function() {
        var input = this.get(0);
        if (!input) return; // No (input) element found
        if ('selectionStart' in input) {
            // Standard-compliant browsers
            return input.selectionStart;
        } else if (document.selection) {
            // IE
            input.focus();
            var sel = document.selection.createRange();
            var selLen = document.selection.createRange().text.length;
            sel.moveStart('character', -input.value.length);
            return sel.text.length - selLen;
        }
    }
})(jQuery);

function getUrlParams() {
	  // This function is anonymous, is executed immediately and 
	  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
        // If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = decodeURIComponent(pair[1]);
        // If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
      query_string[pair[0]] = arr;
        // If third or later entry with this name
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]));
    }
  } 
  return query_string;
};
