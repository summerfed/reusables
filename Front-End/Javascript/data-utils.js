/**
 * Change data to currency format (2 decimal places with comma separator)
 * 		sample usage = Number(data).formatMoney(2)
 * @param c = number of decimal places
 * @param d = decimal symbol character, default is .
 * @param t = separator symbol, default is ,
 * @returns
 */
Number.prototype.formatMoney = function(c, d, t){
var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };
 
 /**
  * Narrow an array based on set filter.
  */
 var Narrower = (function ( window, document, undefined ) {

	    function Narrower( arr ) {
	        this.arr  = arr || []
	    }

	    Narrower.prototype.update = function ( str ) {
	        var rgxp, results

	        // optimization
	        if ( '' === str ) {
	            return this.arr
	        }
	        else {
	            // create regular expression
	            rgxp = new RegExp( str, 'i' ) // todo: make Unicode-safe once ES6 is widely adopted

	            results = this.arr.filter(function ( val ) {
	                return null !== val.match( rgxp )
	            })

	            return results.sort()
	        }
	    }

	    return Narrower

	}).call( this, this, this.document );
 
 /**
  * Get Query Parameters of current page Url
  * @returns Javascript Object wherein query param keys are property names
  * for example: if you have url: http://localhost:8080/reusable?position=Front End Developer&function=getQueryParameter
  * and your implementation is:
  * var queryParams = getUrlParams();
  * to get value of position use: queryParams.position
  */
 function getUrlParams() {
	  // This function is anonymous, is executed immediately and 
	  // the return value is assigned to QueryString!
	 var query_string = {};
	 var query = window.location.search.substring(1);
	 var vars = query.split("&");
	 for (var i=0;i<vars.length;i++) {
	   var pair = vars[i].split("=");
	       // If first entry with this name
	   if (typeof query_string[pair[0]] === "undefined") {
	     query_string[pair[0]] = decodeURIComponent(pair[1]);
	       // If second entry with this name
	   } else if (typeof query_string[pair[0]] === "string") {
	     var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
	     query_string[pair[0]] = arr;
	       // If third or later entry with this name
	   } else {
	     query_string[pair[0]].push(decodeURIComponent(pair[1]));
	   }
	 } 
	 return query_string;
};

/**
 * Get Query Parameters from Given Url
 * @param data
 * @returns Javascript Object wherein query param keys are property names
 * for example: if you have url: http://localhost:8080/reusable?position=Front End Developer&function=getQueryParameter
 * and your implementation is:
 * var queryParams = getUrlParams('http://localhost:8080/reusable?position=Front End Developer&function=getQueryParameter');
 * to get value of position use: queryParams.position
 */
function getUrlParams(data) {
	  // This function is anonymous, is executed immediately and 
	  // the return value is assigned to QueryString!
	 var query_string = {};
	 var oldUrl = data;
	 var newUrl = oldUrl.substring(oldUrl.indexOf('?')+1,oldUrl.length);
	 var query = newUrl;
	 var vars = query.split("&");
	 for (var i=0;i<vars.length;i++) {
	   var pair = vars[i].split("=");
	       // If first entry with this name
	   if (typeof query_string[pair[0]] === "undefined") {
	     query_string[pair[0]] = decodeURIComponent(pair[1]);
	       // If second entry with this name
	   } else if (typeof query_string[pair[0]] === "string") {
	     var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
	     query_string[pair[0]] = arr;
	       // If third or later entry with this name
	   } else {
	     query_string[pair[0]].push(decodeURIComponent(pair[1]));
	   }
	 } 
	 return query_string;
};

/**
 * Generate random String with values based on given possibleCharacters and given length
 * @param possibleCharacters = if you want to generate all numbers enter = '1234567890'
 * @param length
 * @returns {String}
 */
function generateRandomData(possibleCharacters, length) {
    var text = "";
    
    length = Number(length);
    var possibleCharacterLength = possibleCharacters.length;
    for( var i=0; i < length; i++ )
        text += possibleCharacters.charAt(Math.floor(Math.random() * possibleCharacterLength));
    return text;
}

