This README file explains what are the contents of this reusable front-end codes.
Author: Fed Manangan
Co-Author/s:
Date Created: 2016-12-20 09:48AM
Last Modified: 2016-12-21 01:59PM

How to use in Liferay Portlet:
Make sure you include jquery javascript file together with chosen module javascript file in liferay-portlet.xml. Example shown below:

<portlet>
	<portlet-name>reusable-implementation-sample</portlet-name>
	<icon>/icon.png</icon>
	<header-portlet-css>/css/main.css</header-portlet-css>
	
	/** THIS IS THE JQUERY IMPORT
	<footer-portlet-javascript>
		/js/Front-End/Javascript/jquery-2.2.3.min.js
	</footer-portlet-javascript>
	
	/** THIS MODULE JAVASCRIPT IMPORT
	<footer-portlet-javascript>
		/js/Front-End/Javascript/inputbox-formatter.js
	</footer-portlet-javascript>
	
	/** THIS MODULE JAVASCRIPT DEPENDENCY IMPORT
	<footer-portlet-javascript>
		/js/Front-End/Javascript/data-utils.js
	</footer-portlet-javascript>
	<css-class-wrapper>
		reusable-implementation-sample-portlet
	</css-class-wrapper>
</portlet>
 
LEGEND:
values enclosed with {} will serve as parameters and will be replaced with specific values.

Start of Module: =={module title}==
* : function

==INPUT FORMATTER==
description: Contains reusable javascript codes that can be used to format input boxes for proper validation, limitations and accepted keyboard values. 
file location: /Front-End/Javascript/inputbox-formatter.js 
dependencies: 
--jquery-2.2.3.min.js or Higher
--event-utils.js
--data-utils.js

*setInputBoxCharLimit - Limit number of accepted characters of HTML input boxes.
*allowCharacters - Limit what characters the box will accept based on provided parameters.
*setAsMobileNumberTextBox - Make input box mobile number formatted, ensure country code is present on blur if enabled
*setAsNameTextBox - Set input box to accept letter input and symbols: -#.�� only, these are the values set as standard for names.
*setAsCurrencyTextBox - Set input box to accept numbers and decimal point input only, add comma separator as you type values.

==EVENT UTILITY==
description: Contains reusable javascript codes that can be used to for event handling. 
file location: /Front-End/Javascript/event-utils.js 
dependency: jquery-2.2.3.min.js or Higher

*getKeyCode - Cross-browser and mobile friendly function to get keyboard event keyCode.

==DATA UTILITY==
description: Contains reusable javascript codes that can be used to format data. 
file location: /Front-End/Javascript/inputbox-formatter.js 
dependency: jquery-2.2.3.min.js or Higher

*formatMoney - Change data to currency format (2 decimal places with comma separator)
*Narrower - Narrow an array based on set filter
*getUrlParams - Get Query Parameters from Given Url
*generateRandomData - Generate random String with values based on given possibleCharacters and given length