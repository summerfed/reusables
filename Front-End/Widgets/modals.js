/**
 * Get parent folder of executing script, to be used for dependency input
 */
var scriptParentFolder = document.getElementsByTagName("script"),
	src = scriptParentFolder[scriptParentFolder.length-1].src;
	scriptParentFolder = src.substring(0,src.lastIndexOf('/')) + '/';

/**
 * Import data-utils.js
 */
var importDataUtils = document.createElement('script');
importDataUtils.src = scriptParentFolder + 'data-utils.js';
document.head.appendChild(importDataUtils);

var LoadingModal = (function ( window, document, undefined ) {
	var thisModalElement;
	var loadingTimeout;
	var timeoutErrorModal;
	/**
	 * Call Loading Modal
	 * @param imageSrc = Location of Image Animation to use for loading
	 * @param timeoutErrorTimer = if present loading modal will display error after timeout, time in millis.
	 */
    function LoadingModal(imageSrc, timeoutErrorTimer) {
        this.imageSrc  = imageSrc;
        timeoutErrorTimer = Number(timeoutErrorTimer);
        var generatedId = generateRandomData('1234567890', 10);
        var thisModalId = 'loading-'+generatedId;
        var loadingScreenHtml = '<div id="'+thisModalId+'" style=" width: 100%;height: 100%;top: 0;left: 0;position: fixed;opacity: 1;background-color: rgba(0,0,0, .2);z-index: 99;text-align: center;">'+
		'<img style=" position: absolute;top: 50%;left: 50%;z-index: 100;" id="loading-image-'+generatedId+'" src="'+imageSrc+'" alt="Loading..." />'+
		'</div>';
    	$('body').append(loadingScreenHtml);
    	thisModalElement = $('#'+thisModalId);
    	
    	//Center Image On Screen
	    $("#loading-image-"+generatedId).load(function() {
	        var imgHeight = $(this).height();
	        var imgWidth = $(this).width();
	        var marginLeftSize = '-'+(imgWidth/2)+'px';
	        var marginTopSize = '-'+(imgHeight/2)+'px';
	        
	        $(this).css('margin-left', marginLeftSize);
	        $(this).css('margin-top', marginTopSize);
	    });
    	
        if(!isNaN(timeoutErrorTimer)) {
			 loadingTimeout = setTimeout(function() {
			 	var isLoadingExist = thisModalElement.css('display');
			 	if(isLoadingExist == 'block'){
			 		thisModalElement.css('display','none');
			 		 if($( "#timeoutError-"+generatedId ).length==0) {
			 			 var timeoutError = '<div id="timeoutError-'+generatedId+'" style="   color: white; width: 100%;height: 100%;top: 0;left: 0;position: fixed;opacity: 1;background-color: rgba(0,0,0, .2);z-index: 99;text-align: center;">'+
							'<div id="timeoutErrorContent-'+generatedId+'" style=" background-color: gray;padding: 20px; position: absolute;top: 50%;left: 50%;margin-left: -135px;margin-top: -100px;z-index: 100;"><p>An error has occured please try again.</p> <button type="button" id="closeLoadingError-'+generatedId+'">OK</button></div>'+
							'</div>';
			 			 
			 			 $('body').append(timeoutError);
			 			 timeoutErrorModal = $('#timeoutError-'+generatedId);
			 			 $('#closeLoadingError-'+generatedId).click(function(){
			 				$('#timeoutError-'+generatedId).css('display', 'none'); 
			 			 });
			 		 }
			 		 $('#timeoutError-'+generatedId).css('display','block');
			 	}
			 }, timeoutErrorTimer);
        }
    }
    
    /**
     * Display Loading Modal
     */
	 LoadingModal.prototype.show = function () {
    	thisModalElement.css('display','block');
	 }
	 
	 /**
	  * Hide Loading Modal, Hidden Modal can be displayed again by calling show function
	  */
	 LoadingModal.prototype.hide = function () {
    	thisModalElement.css('display','none');
    	clearTimeout(loadingTimeout);
	 }
	 
	 /**
	  * Remove Modal Object from the DOM
	  */
	 LoadingModal.prototype.destroy = function () {
    	thisModalElement.remove();
    	if(timeoutErrorModal!==undefined) {
    		timeoutErrorModal.remove();
    	}
	 }

    return LoadingModal
}).call( this, this, this.document ); 

/*var loadingTimeout;
 
 function showLoadingScreen(imageSrc) {
	 
	 var loadingScreenHtml = '<div id="loading" style=" width: 100%;height: 100%;top: 0;left: 0;position: fixed;opacity: 1;background-color: rgba(0,0,0, .2);z-index: 99;text-align: center;">'+
							 	'<img style=" position: absolute;top: 50%;left: 50%;margin-left: -140px;margin-top: -120px;z-index: 100;" id="loading-image" src="'+imageSrc+'" alt="Loading..." />'+
							 '</div>';
	 
	 if($( "#loading" ).length==0) {
		 $('body').append(loadingScreenHtml);
	 }
	 $('#loading').css('display','block');
	 
	 loadingTimeout = setTimeout(function() {
	 	var isLoadingExist = $('#loading').css('display');
	 	if(isLoadingExist == 'block'){
	 		hideLoadingScreen();
	 		 if($( "#timeoutError" ).length==0) {
	 			 var timeoutError = '<div id="timeoutError" style="   color: white; width: 100%;height: 100%;top: 0;left: 0;position: fixed;opacity: 1;background-color: rgba(0,0,0, .2);z-index: 99;text-align: center;">'+
					'<div id="timeoutErrorContent" style=" background-color: gray;padding: 20px; position: absolute;top: 50%;left: 50%;margin-left: -135px;margin-top: -100px;z-index: 100;"><p>An error has occured please try again.</p> <button type="button" id="closeLoadingError">OK</button></div>'+
					'</div>';
	 			 
	 			 $('body').append(timeoutError);
	 			 
	 			 $('#closeLoadingError').click(function(){
	 				$('#timeoutError').css('display', 'none'); 
	 			 });
	 		 }
	 		 $('#timeoutError').css('display','block');
	 	}
	 }, 60000);
 }
 
 function hideLoadingScreen() {
	 $('#loading').css('display','none');
	 clearTimeout(loadingTimeout);
 }*/